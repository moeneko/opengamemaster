
import pickle
from langchain_openai import ChatOpenAI
from langchain.chains import LLMChain
from langchain.memory import ConversationBufferMemory,ConversationSummaryBufferMemory
from langchain_community.callbacks import get_openai_callback
from langchain.prompts import PromptTemplate
from config import config

class openGameMaster():
    """
    A class used to manage GameMaster methods and bot commands.

    Attributes
    ----------
    game_identifier : list
        list of game identifiers, suggested as platform + server and channel id,
        will be used as part of the file name and key for finding the right context
    
    Methods
    -------
    """

    def __init__(self,chain_file,openai_api_key,model_name,gametype,temperature=1):
        self.chain_file = chain_file
        self.openai_api_key = openai_api_key
        self.model_name = model_name
        self.temperature = temperature
        self.gametype = gametype
        self.majorplot = ''
        self.minorplot = ''
        self.playersdet= ''
        self.llm = ChatOpenAI(
        	temperature=self.temperature,
        	openai_api_key=self.openai_api_key,
        	model_name=self.model_name
        )
        try:
            self.conchain = self.load_buffer(chain_file)
        except Exception as e:
            print("Error loading previous data: " + str(e))
            self.conchain = self.create_chain(self.create_buffer())
    

    def create_buffer(self):
        return ConversationBufferMemory()

    def create_chain(self,con_buffer):
        return LLMChain(
            llm=self.llm,
            memory=con_buffer,
            verbose=True,
            prompt=self.prompt_template(self.base_prompt(self.gametype,self.majorplot,self.minorplot,self.playersdet))
        )

    def save_buffer(self):
        """
        Saves ConversationBuffMemory() to buffer_file 
        """
        with open(self.chain_file, 'wb') as s_file:
            pickle.dump(self.conchain.memory,s_file)
    
    def load_buffer(self,buffer_file):
        """
        Creates ConversationBuffMemory() with conects of file from save_buffer()
        """
        with open(buffer_file, 'rb') as m_file:
            memory_data = pickle.load(m_file)
            conversation_memory = self.create_chain(self.create_buffer())
            conversation_memory.memory = memory_data
            return conversation_memory

    def update_buffer_user(self,this_update):
        self.conchain.memory.chat_memory.add_user_message(this_update)

    def update_buffer_ai(self,this_update):
        self.conchain.memory.chat_memory.add_ai_message(this_update)

    def create_summary(self,previous_summary = '',max_token_limit=3000,):
        con_sum_mem = ConversationSummaryBufferMemory(
                llm=self.llm,
                max_token_limit=max_token_limit
            )
        return con_sum_mem.predict_new_summary(self.conchain.memory.chat_memory.messages,previous_summary)
    def run_it(self):
        return self.conchain.run()
    def show_history(self):
        return self.conchain.memory.chat_memory.messages
        
    def create_example_prompt(self):
        # create a example template
        example_template = """
        User: {query}
        AI: {answer}
        """
        
        # create a prompt example from above template
        example_prompt = PromptTemplate(
            input_variables=["query", "answer"],
            template=example_template
        )
        return example_prompt

    def base_prompt(self,gametype,majorplot,minorplot,playersdet):
        return f"""You are a GM (Game Master) of the game {gametype}. Only do actions of the GM and NPCs, never the players. Always ask players what they are doing.
Do not roll for players, Ask for dice rolls for actions.
The plot, game setting, and any crucial NPCs will be provided under the "The plot of the game is" section for each session. The team will work together to complete various objectives while facing challenges in a dynamic world that incorporates a mix of character archetypes, tones, and atmospheres.
Only add one line after GM: 

Example context:
(Player)Name - Class:  I like to travel.
GM: There are many places to explore.
(NPC)Character: Where would you like to go?
GM: Roll for Local Expert to see how much you know about the area.

The major plot of the game is {majorplot}

The minor plot of the game is {minorplot}

Players:
----
{playersdet}
        
The happenings so far:
----
"""
    def prompt_template(self,pretext):
        template = pretext + """

Previous conversation:
{chat_history}

{question}
GM:"""
        return PromptTemplate.from_template(template)

    def count_tokens(self,chain, query):
       with get_openai_callback() as cb:
           result = chain.run(query)
           print(f'Spent a total of {cb.total_tokens} tokens')
   
       return result

# Main process for testing
if __name__ == '__main__':
    chain_file = '/tmp/chaintest.pkl'
    oGM = openGameMaster(chain_file,config.OPENAI_API_KEY,'gpt-4-1106-preview',"Dungeons and Dragons",1)
    text_input = ''
    while text_input != 'exit':
        text_input = str(input(":"))
        if text_input != 'exit':
            oGM.update_buffer_user(text_input)
            oGM.save_buffer()
    print(oGM.show_history())
    print(oGM.create_summary())

