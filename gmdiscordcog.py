import re
import os
import json
from discord.ext import commands
from config import config
from gamemaster import openGameMaster

class gamemaster_discord_cog(commands.Cog):
    """ 
    Attributes
    ----------
    bot : discord.ext.commands.Bot() 
        the bot object from discord.py
    botconfig : dict
        The bot configuration, takes in a json file or creates base dictionary
    
    Commands
    --------
    start
        starts a new game
    plot
        sets the game plot
    limit
        sets game history limit
    add
        adds a player
    desc
        adds player description
    deletegame
        delets current game
    gm
        interacts with game master ai
    newplot
        has game master create new plot
    roll
        roll dice
    """


    def __init__(self, bot):
        """
        Sets up openGameMaster class. Reads in config file and botconfig variable if no config is available.
        
        Parameters
        ----------
        bot : discord.ext.commands.Bot() 
            bot from discord.ext commands
        """
        self.bot = bot
        self.gamemaster = openGameMaster(bot.guilds)

    @commands.command(name='start',help='intial bot setup')
    async def _startbot(self,ctx):
        if not self.gamemaster.checkGame(str(ctx.guild.id)):
            self.gamemaster.checkGame(str(ctx.guild.id),True)
            self.bot.gmplayers[str(ctx.guild.id)] = {}
            await ctx.send('Setting up bot, Please enter type of game: ')
            response = await self.bot.wait_for('message')
            gametype = response.content
            gametypeset = self.gamemaster.setGameType(gametype,str(ctx.guild.id),str(ctx.channel.id))
            self.bot.gmchannels[str(ctx.guild.id)] = str(ctx.channel.id)
            await ctx.send(gametypeset + """ set as game type, This channel is now intialized for use.
To change re-run this command.
                       
Use """ + config.BOT_PREFIX + """add command to add players, and """ + config.BOT_PREFIX + " to set the plot")
        else:
            await ctx.send('Game already exists, uses `' + config.BOT_PREFIX + 'deletegame` to delete the current game.')

    @commands.command(name='plot',help='Set game plot')
    async def _plotbot(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            await ctx.send('In 200 words or less, Please enter a plot for the game: ')
            response = await self.bot.wait_for('message')
            plot = response.content
            self.gamemaster.setPlot(plot,str(ctx.guild.id))
            self.bot.gmchannels[str(ctx.guild.id)] = str(ctx.channel.id)
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')
    @commands.command(name='limit',help='Set game history limit')
    async def _histlimit(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            self.gamemaster.setLimit(str(ctx.guild.id),str(ctx.message.content))
            await ctx.send('Set history limit to: ' + str(int(re.sub('[^0-9]','',ctx.message.content))))
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')

    @commands.command(name='add',help='Add or replace player')
    async def _addplayer(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            await ctx.send('What is your players name?')
            response1 = await self.bot.wait_for('message')
            pname = response1.content
            await ctx.send('What is your players class?')
            response2 = await self.bot.wait_for('message')
            pclass = response2.content
            await ctx.send('Please describe your character, include level. and a physical description:')
            response3 = await self.bot.wait_for('message')
            pdesc = response3.content.strip()
            self.gamemaster.addPlayer(str(ctx.guild.id),str(ctx.author.id),pname,pclass,pdesc)
            self.bot.gmplayers[str(ctx.guild.id)][str(ctx.author.id)] = {}
            self.bot.gmplayers[str(ctx.guild.id)][str(ctx.author.id)]['nameclass'] = pname + ' - ' + pclass
            self.bot.gmplayers[str(ctx.guild.id)][str(ctx.author.id)]['description'] = pdesc
            await ctx.send('Added player ' + pname + ' class ' + pclass + ' as you')
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')
    @commands.command(name='desc',help='Update player description')
    async def _descplayer(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            pnameclass = self.bot.gmplayers[str(ctx.guild.id)][str(ctx.author.id)]
            await ctx.send('Updating the description for ' + pnameclass['nameclass'])
            await ctx.send('Please describe your character, include level. and a physical description:')
            response3 = await self.bot.wait_for('message')
            pdesc = response3.content.strip()
            pname,pclass = pnameclass['nameclass'].split(' - ',1)
            self.gamemaster.addPlayer(str(ctx.guild.id),str(ctx.author.id),pname,pclass,pdesc)
            await ctx.send('Updated player ' + pname + ' class ' + pclass + ' as you')
            self.bot.gmplayers[str(ctx.guild.id)][str(ctx.author.id)] = {
                 "nameclass":pnameclass['nameclass'],
                 "description":pdesc
            }
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')
    @commands.command(name='deletegame',help='Deletes game')
    async def _deletegame(self,ctx):
        self.gamemaster.deleteGame(str(ctx.guild.id))
        await ctx.send(self.gamemaster.botconfig[str(ctx.guild.id)]['gametype'] + ' game deleted.')
        self.gamemaster.botconfig[str(ctx.guild.id)] = {
            "gametype":"",
            "channelid":"",
            "plot":"unset.",
            "majorplot":"unset.",
            "players":{}
        }
        self.gamemaster.checkGame(str(ctx.guild.id))
        self.bot.gmplayers[str(ctx.guild.id)] = {}
        self.bot.gmchannels[str(ctx.guild.id)] = ''
    @commands.command(name='gm',help='Interact with GameMaster')
    async def _gmgame(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            await ctx.send(self.gamemaster.aiGM(str(ctx.guild.id),str(ctx.author.id),str(ctx.message.content)))
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')
        
    @commands.command(name='newplot',help='Have the GM create a new plot')
    async def _gmnewplot(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            await ctx.send(self.gamemaster.aiGM(str(ctx.guild.id),str(ctx.author.id),str(ctx.message.content),True))
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')
    @commands.command(name='save',help='Save game')
    async def _gmsavegame(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            try:
                save_name_pre = ' '.join(ctx.message.content.split(" ")[1:])
                save_name = re.sub("_","",save_name_pre)
                if self.gamemaster.saveGame(str(ctx.guild.id),save_name):
                    await ctx.send('Saved game: ' + save_name)
            except:
                await ctx.send('Include save game name: `' + config.BOT_PREFIX + 'save Some Save Name`')
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')
    @commands.command(name='load',help='Load saved game')
    async def _gmloadgame(self,ctx):
        try:
            save_name = ' '.join(ctx.message.content.split(" ")[1:])
            cplayers,cchannelid = self.gamemaster.loadGame(str(ctx.guild.id),save_name)
            self.bot.gmplayers[str(ctx.guild.id)] = cplayers
            self.bot.gmchannels[str(ctx.guild.id)] = cchannelid
            await ctx.send('Loaded game: ' + save_name)
        except Exception as e:
            print(e)
            await ctx.send('Game not found. Include save game name: `' + config.BOT_PREFIX + 'load Some Save Name` must match save')
            list_of_games = []
            for save_file_name in os.listdir(config.BOT_STORAGE + '/save/'):
                if re.match('^gm_config',save_file_name):
                    game_name_pre = ' '.join(save_file_name.split("_")[3])
                    game_name = ''.join(game_name_pre.split('.')[0])
                    list_of_games.append(game_name)
            if len(list_of_games) > 0:
                await ctx.send('List of games found:')
                await ctx.send("\n".join(list_of_games))
    @commands.command(name='roll',help='Roll dice')
    async def _gmroll(self,ctx):
        if self.gamemaster.checkGame(str(ctx.guild.id)):
            try:
                roll_text = ''.join(ctx.message.content.split(" ")[1])
                roll_bonus = int(''.join(ctx.message.content.split(" ")[2]))
                roll_effect = ' '.join(ctx.message.content.split(" ")[3:])
                roll_effect = roll_effect.strip()
                dice_result,dice_math = self.gamemaster.game_roll(roll_text,roll_bonus)
                roll_player = self.bot.gmplayers[str(ctx.guild.id)][str(ctx.author.id)]['nameclass']
                game_message = roll_player + ' rolled a ' + dice_result + ' on ' + roll_effect + ' ||' + dice_math + '||'
                with open(config.BOT_STORAGE + '/gm_history_' + str(ctx.guild.id) + '.json','a') as histfile:
                    histfile.write(game_message + "\n")
                await ctx.send(game_message)
            except Exception as e:
                print(e)
                await ctx.send('Must be in `' + config.BOT_PREFIX + 'roll 1d20 4 Stealth` FORMAT, where 1d20 is the dice to roll, 4 is the modifier and Stealth is the skill')
        else:
            await ctx.send('No game found, run ' + config.BOT_PREFIX + 'start command')

async def setup(bot):
    await bot.add_cog(gamemaster_discord_cog(bot))