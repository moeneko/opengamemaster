import os
import discord
import re
import json
from discord.ext import commands
from config import config
from gmdiscordcog import gamemaster_discord_cog

initial_extensions = ['gmdiscordcog']
print(config.BOT_PREFIX)
intents = discord.Intents(messages=True, guilds=True, message_content=True)
bot = commands.Bot(command_prefix=config.BOT_PREFIX,
                   pm_help=True, case_insensitive=True,intents=intents)

if __name__ == '__main__':

    config.ABSOLUTE_PATH = os.path.dirname(os.path.abspath(__file__))
    config.COOKIE_PATH = config.ABSOLUTE_PATH + config.COOKIE_PATH

    if config.BOT_TOKEN == "":
        print("Error: No bot token!")
        exit

bot.gmchannels = {}
bot.gmplayers = {}

@bot.event
async def on_ready():
    files = [f for f in os.listdir(config.BOT_STORAGE) if re.match('^gm_config_.*',f)]
    for file in files:
        guildid = re.sub('.json','',re.sub('gm_config_','',file))
        with open(config.BOT_STORAGE + '/' + file,'r') as fileo:
            bdata = json.load(fileo)
            bot.gmchannels[guildid] = bdata['channelid']
            bot.gmplayers[guildid] = bdata['players']
    for extension in initial_extensions:
        try:
            await bot.load_extension(extension)
        except Exception as e:
            print(e)
    print(config.STARTUP_MESSAGE)
    await bot.change_presence(status=discord.Status.online, activity=discord.Game(name="GameMaster, type {}help".format(config.BOT_PREFIX)))

    for guild in bot.guilds:
        if str(guild.id) not in bot.gmplayers.keys():
            bot.gmplayers[str(guild.id)] = {}
        
        print("Joined {}".format(guild.name))

    print(config.STARTUP_COMPLETE_MESSAGE)

@bot.event
async def on_message(message):
    if not message.author.bot:
        cunprocessed = True
        if str(message.guild.id) in bot.gmchannels.keys():
            if str(message.channel.id) == bot.gmchannels[str(message.guild.id)]:
                print(bot.gmplayers[str(message.guild.id)])
                if str(message.author.id) in bot.gmplayers[str(message.guild.id)].keys():
                    if not message.content.startswith(config.BOT_PREFIX) and not message.content.startswith("http"):
                        game_message = '(Player)' + bot.gmplayers[str(message.guild.id)][str(message.author.id)]['nameclass'] + ": " + message.content
                        with open(config.BOT_STORAGE + '/gm_history_' + str(message.guild.id) + '.json','a') as histfile:
                            histfile.write(game_message.strip() + "\n")
                    else:
                        await bot.process_commands(message)
                        cunprocessed = False
                elif not re.match('^.add',message.content):
                    await message.channel.send("Add yourself to the game before using bot, with the `" + config.BOT_PREFIX + "add` command")
                    cunprocessed = False
                elif re.match('^.add',message.content):
                    bot.gmplayers[str(message.guild.id)][str(message.author.id)] = {'nameclass':'','description':''}

        if cunprocessed and (config.BOT_PREFIX + 'start' or config.BOT_PREFIX + 'add' or config.BOT_PREFIX + 'load'):
            await bot.process_commands(message)
        elif cunprocessed:
            await message.channel.send('Start a game with ' + config.BOT_PREFIX + 'start or add yourself to the current game with ' + config.BOT_PREFIX + 'add')


bot.run(config.BOT_TOKEN, reconnect=True)