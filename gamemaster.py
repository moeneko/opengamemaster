import os
import json
import re
import requests
import dice
import shutil
from pathlib import Path
from config import config
from langchain.memory import ConversationBufferMemory

#####
# By Lucid1#0001@Discord
#####


class openGameMaster():
    """
    A class used to manage GameMaster methods and bot commands.

    Attributes
    ----------
    game_identifier : list
        list of game identifiers, suggested as platform + server and channel id,
        will be used as part of the file name and key for finding the right context
    
    Methods
    -------
    config_file(game_identifier)
        provides the game master config file location and name
    history_file(game_identifier)
        provides the history file location and name
    log(type,msg)
        for logging messages
    checkGame(game_identifier: str,create = False) -> bool:
        checks if game files exist, and can create them
    createGMConfig(game_identifier)
        updates the game master config file
    createGMHistory(game_identifier)
        creates empty history file
    setGameType(gametype,plot,ctx)
        prompts players for game details and creates config
    setPlot(plot,ctx)
        updates game plot
    addPlayer(ctx,name,pclass)
        adds player to the game
    deleteGame(ctx)
        deletes game by removing game files and genereating new ones
    aiGM(ctx,newplot)
        interacts with ai language model with game details and history
       """
    def __init__(self,game_identifiers : list):
        """
        Attributes
        ----------
        game_identifier : list
            list of game identifiers, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        """
        self.botconfig = {}
        for game_identifier_any in game_identifiers:
            game_identifier = str(game_identifier_any)
            bot_config_file = config.BOT_STORAGE + '/' + 'gm_config_' + game_identifier + '.json'
            bot_history_file = config.BOT_STORAGE + '/' + 'gm_history_' + game_identifier + '.json'
            if not Path(bot_config_file).is_file():
                self.botconfig[game_identifier] = {
                    "gametype":"",
                    "channelid":"",
                    "histlimit":60,
                    "plot":"unset.",
                    "majorplot":"unset.",
                    "players":{}
                }
            else:
                with open(bot_config_file) as botconfile:
                    self.botconfig[game_identifier] = json.load(botconfile)
    def readConfig(self,game_identifier : str) -> dict:
        bot_config_file = config.BOT_STORAGE + '/' + 'gm_config_' + game_identifier + '.json'
        if not Path(bot_config_file).is_file():
            self.botconfig[game_identifier] = {
                "gametype":"",
                "channelid":"",
                "histlimit":60,
                "plot":"unset.",
                "majorplot":"unset.",
                "players":{}
            }
        else:
            with open(bot_config_file) as botconfile:
                    self.botconfig[game_identifier] = json.load(botconfile)
        return self.botconfig[game_identifier]
    def config_file(self,game_identifier : str) -> str:
        """
        Returns the config file location and name

        Parameters
        ----------
        game_identifier 
            id of the current guild interacting with the bot
        """
        bot_config_file = config.BOT_STORAGE + '/' + 'gm_config_' + str(game_identifier) + '.json'
        return bot_config_file
    def new_memory(self):
        return ConversationBufferMemory()
    def new_llm(self:
        llm = OpenAI(
	        temperature=0,
	        openai_api_key="OPENAI_API_KEY",
	        model_name="text-davinci-003"))
    def history_file(self,game_identifier : str) -> str:
        """
        Returns the history file location and name

        Parameters
        ----------
        game_identifier 
            id of the current guild interacting with the bot
        """
        bot_history_file = config.BOT_STORAGE + '/' + 'gm_history_' + str(game_identifier) + '.json'
        return bot_history_file
    def log(self,type : str,msg : str):
        """
        Handles log messages

        Parameters
        ----------
        type : str
            type of log (Error, Info, Etc)
        msg : str
            message or context of log
        """
        print(str(type) + ' - ' + str(msg))
    def checkGame(self,game_identifier: str,create = False) -> bool:
        """
        Verifies if config and history files exist, can also create them

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        create : bool, optional
            Whether to create the conig and history files
        """
        bot_config_path = Path(self.config_file(game_identifier))
        if not bot_config_path.is_file():
            if create == True:
                self.createGMConfig(game_identifier)
            else:
                return False
        bot_history_path = Path(self.history_file(game_identifier))
        if not bot_history_path.is_file():
            if create == True:
                self.createGMHistory(game_identifier)
            else:
                return False
        return True
    def saveGame(self,game_identifier : str, save_name : str) -> bool:
        """
        Saves game

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        save_name : str
            Name to use for the save
        """
        save_dir = config.BOT_STORAGE + '/save/'
        config_save_location = config.BOT_STORAGE + '/save/' + 'gm_config_' + str(game_identifier) + '_' + save_name + '.json'
        history_save_location = config.BOT_STORAGE + '/save/' + 'gm_history_' + str(game_identifier) + '_' + save_name + '.json'
        try:
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)
            shutil.copyfile(self.config_file(game_identifier),config_save_location)
            shutil.copyfile(self.history_file(game_identifier),history_save_location)
            return True
        except Exception as e:
            print(e)
            return False
    def loadGame(self,game_identifier : str, save_name : str) -> bool:
        """
        Loads game

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        save_name : str
            Name to use for the save
        """
        save_dir = config.BOT_STORAGE + '/save/'
        config_save_location = config.BOT_STORAGE + '/save/' + 'gm_config_' + str(game_identifier) + '_' + save_name + '.json'
        history_save_location = config.BOT_STORAGE + '/save/' + 'gm_history_' + str(game_identifier) + '_' + save_name + '.json'
        try:
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)
            if Path(config_save_location).is_file() and Path(history_save_location).is_file():
                shutil.copyfile(config_save_location,self.config_file(game_identifier))
                shutil.copyfile(history_save_location,self.history_file(game_identifier))
                self.botconfig[game_identifier] = self.readConfig(game_identifier)
                return self.botconfig[game_identifier]['players'],self.botconfig[game_identifier]['channelid']
            else:
                return False
        except Exception as e:
            print(e)
            return False
        
        

    def createGMConfig(self,game_identifier : str):
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        """
        Creates GM Config file with contents of self.botconfig[game_identifier]

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        """
        with open(self.config_file(game_identifier), 'w') as botconfig:
            botconfig.write(json.dumps(self.botconfig[game_identifier]))
    def updateGMConfig(self,botconfig : dict, game_identifier : str):
        with open(self.config_file(game_identifier), 'w') as botconfigfile:
            botconfigfile.write(json.dumps(botconfig[game_identifier]))
    def createGMHistory(self,game_identifier : str):
        """
        Creates blank GM History file

        Parameters
        ----------
        game_identifier
            Guild ID of context
        """
        with open(self.history_file(game_identifier), 'w') as botconfig:
            pass
    def setGameType(self,gametype : str,game_identifier : str,sub_identifier : str) -> str:
        """
        Sets up the game config, and writes it to a file

        Parameters
        ----------
        gametype : str
            The type of Role playing game to be played eg. Dungeons and Dragons
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        sub_identifier : str
            channel identification, allows multiple games, one per channel
        """
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        self.botconfig[game_identifier]['gametype'] = gametype
        self.botconfig[game_identifier]['channelid'] = sub_identifier
        with open(self.config_file(game_identifier), 'w') as botconfig:
            botconfig.write(json.dumps(self.botconfig[game_identifier]))
        return 'Game type set to ' + gametype
    def setPlot(self,plot : str, game_identifier : str):
        """
        Sets or Changes the Plot of the current game session

        Parameters
        ----------
        plot : str
            The plot of the current scenario
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        """
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        self.botconfig[game_identifier]['plot'] = plot
        self.updateGMConfig(self.botconfig,game_identifier)
    def addPlayer(self,game_identifier : str,pid : str,name : str,pclass :str, pdesc:str):
        """
        Adds a player to the current session

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        pid : str
            players identity
        name : str
            Name of the player to add
        pclass : str
            Class of the player to add
        pdesc : str
            Description of the player
        """
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        self.botconfig[game_identifier]['players'][str(pid)] = {}
        self.botconfig[game_identifier]['players'][str(pid)]['nameclass']  = name + ' - ' + pclass
        self.botconfig[game_identifier]['players'][str(pid)]['description']  = pdesc
        self.updateGMConfig(self.botconfig,game_identifier)
    def deleteGame(self,game_identifier : str):
        """
        Delete current game session.

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        """
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        try:
            os.unlink(self.config_file(game_identifier))
            os.unlink(self.history_file(game_identifier))
        except:
            pass
    def setLimit(self,game_identifier : str,new_limit : str):
        """
        Sets chat history limit

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        """
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        self.botconfig[game_identifier]['histlimit'] = int(re.sub('[^0-9]','',new_limit))
        self.updateGMConfig(self.botconfig,game_identifier)
    def adjustLimit(self,game_identifier : str,change : int):
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        newlimit = self.botconfig[game_identifier]['histlimit'] + change
        print("New limit set to: " + str(newlimit))
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        self.botconfig[game_identifier]['histlimit'] = int(newlimit)
        self.updateGMConfig(self.botconfig,game_identifier)
    def game_roll(self,msg : str,dmodifier : int):
        if re.match("cpr",msg):
            firstroll = dice.roll('1d10')[0]
            if firstroll == 1:
                secondroll = dice.roll('1d10')[0]
                resultnum = firstroll - secondroll + dmodifier
                resultstr = str(resultnum) + " Critical Miss"
                resultmath = "(" + str(firstroll) + ' - ' + str(secondroll) + " + " + str(dmodifier) + ")"
            elif firstroll == 10:
                secondroll = dice.roll('1d10')[0]
                resultnum = firstroll + secondroll + dmodifier
                resultstr = str(resultnum) + " Critical Success"
                resultmath = "(" + str(firstroll) + ' + ' + str(secondroll) + " + " + str(dmodifier) + ")"
            else:
                resultnum = firstroll + dmodifier
                resultstr = str(resultnum)
                resultmath = "(" + str(firstroll) + ' + ' + str(dmodifier) + ")"
            return resultstr,resultmath
        else:
            try:
                print(msg)
                dres = dice.roll(msg)
                dresrolls = ' + '.join(str(x) for x in dres)
                dresresult = str(sum(dres) + dmodifier)
                if len(dres) > 1:
                    finres = dresresult
                    finmath = "(" + dresrolls + " + " + str(dmodifier) +" modifier)"
                    return finres,finmath
                elif len(dres) == 1:
                    finres = dresresult
                    finmath = "(" + dresrolls + " + " + str(dmodifier) +" modifier)"
                    return finres,finmath
                else:
                    return 'Problem with roll, use 1d20 format'
            except Exception as e:
                print(e)
                return 'Problem with roll, use 1d20 format'
    def aicall(self,msg : str):
        chatres = requests.post(config.LANG_MODEL_URL,data=msg,timeout=60)
        chatdec = json.loads(chatres.text)
        chatmsg = chatdec['choices'][0]['message']['content']
        usage = chatdec['usage']['prompt_tokens']
        hist_over = False
        if(usage > 6000):
            hist_over = True
        print(chatdec)
        return chatmsg, hist_over
    def aiGM(self,game_identifier : str,player_id : str,msg : str,newplot = False,newhist = False,tries = 0):
        """
        Interacts with the language model, This combines the game master config information
        with the history of the session to get a return from the language model.

        Uses a backend webserver to communicate the prompt and get the return.

        Parameters
        ----------
        game_identifier : str
            game identifier, suggested as platform + server and channel id,
            will be used as part of the file name and key for finding the right context
        newplot : bool, optional
            Whether to generate a new plot
        """
        hdata = ""
        self.botconfig[game_identifier] = self.readConfig(game_identifier)
        if(newhist):
            with open(self.history_file(game_identifier),'r') as histfile:
                for line in (histfile.readlines() [:self.botconfig[game_identifier]['histlimit']]):
                    hdata = hdata + line
        else:
            with open(self.history_file(game_identifier),'r') as histfile:
                for line in (histfile.readlines() [self.botconfig[game_identifier]['histlimit'] * -1:]):
                    hdata = hdata + line
        try:
            gmline = '(Player)' + self.botconfig[game_identifier]['players'][player_id]['nameclass'] + ' asks GM: '+ msg[4:] + "\n"  + "GM: "
        except Exception as e:
            print(str(e))
            return "Only players can participate."
        gametype = self.botconfig[game_identifier]['gametype']
        gameplot = self.botconfig[game_identifier]['plot']
        majorplot = self.botconfig[game_identifier]['majorplot']
        playersdet = ''
        for tplayer in self.botconfig[game_identifier]['players'].keys():
            pnameclass = self.botconfig[game_identifier]['players'][str(tplayer)]['nameclass']
            pdescription = self.botconfig[game_identifier]['players'][str(tplayer)]['description']
            playersdet = playersdet + pnameclass  + ' is described as ' + pdescription + "\n" 
        system_gmprompt = f"""You are a GM (Game Master) of the game {gametype}. Only do actions of the GM and NPCs, never the players. Always ask players what they are doing.
Do not roll for players, Ask for dice rolls for actions.
The plot, game setting, and any crucial NPCs will be provided under the "The plot of the game is" section for each session. The team will work together to complete various objectives while facing challenges in a dynamic world that incorporates a mix of character archetypes, tones, and atmospheres.
Only add one line after GM: 

Example context:
(Player)Name - Class:  I like to travel.
GM: There are many places to explore.
(NPC)Character: Where would you like to go?
GM: Roll for Local Expert to see how much you know about the area.

The major plot of the game is {majorplot}

The minor plot of the game is {gameplot}

Players:
----
{playersdet}
        
The happenings so far:
----
"""

        histdata = f"""{hdata}{gmline}"""
        hist_split = histdata.splitlines()
        full_data = [{"role": "system", "content": system_gmprompt}]
        for hist_line in hist_split:
            if(hist_line == ''):
                pass
            elif(re.match('^GM:',hist_line)):
                new_line = {"role":"assistant","content": hist_line}
            else:
                new_line = {"role":"user","content": hist_line}
            if(hist_line != ''):
                full_data.append(new_line)


        if(newplot):
            full_data.append({"role":"user","content":'End Scene. Using the context so far, generate a new minor plot: '})
        if(newhist):
            full_data.append({"role":"user","content":'Using 300 words, please desribe what has happened so far: '})
        gmprompt = json.dumps(full_data) 
        chatgptm = {'prompt':re.sub("'",'',gmprompt)}
        try:
            try:
                chatmsg,hist_over = self.aicall(chatgptm)
                if(hist_over and newhist == False):
                    if self.botconfig[game_identifier]['histlimit'] > 50:
                        self.adjustLimit(game_identifier,-15)
                    self.aiGM(game_identifier,player_id,msg,False,True,tries)
            except Exception as e:
                tries = tries + 1
                if tries < 6:
                    print('Error accessing ai.. retrying')
                    print(e)
                    if self.botconfig[game_identifier]['histlimit'] > 30:
                        self.adjustLimit(game_identifier,-10)
                    chatmsg = self.aiGM(game_identifier,player_id,msg,newplot,newhist,tries)
                else:
                    return "Error accessing language model, please verify it is set up. https://gitlab.com/moeneko/opengamemaster/"
            if(newplot):
                 self.botconfig[game_identifier]['plot'] = chatmsg
                 self.setPlot(chatmsg,game_identifier)
                 self.updateGMConfig(self.botconfig,game_identifier)
                 return 'New plot generated.'
            if(newhist):
                short_hdata = ''
                with open(self.history_file(game_identifier),'r') as histfile:
                    for line in (histfile.readlines() [-45:]):
                        short_hdata = short_hdata + "\n" + line
                short_hdata = "\n".join([s for s in short_hdata.splitlines() if s])
                new_full_hist = 'GM: ' + chatmsg.strip() + "\n"  + short_hdata + "\n"
                with open(self.history_file(game_identifier), 'w') as histfilex:
                    histfilex.write(new_full_hist)
                    return new_full_hist
                
            self.adjustLimit(game_identifier,1)
            with open(self.history_file(game_identifier),'a') as histfile:
                histfile.write(gmline + chatmsg.strip() + "\n")
            return chatmsg
        except Exception as e:
            print(e)
            return "Error accessing language model, please verify it is set up. https://gitlab.com/moeneko/opengamemaster/"