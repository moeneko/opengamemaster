import os
import types
from dotenv import load_dotenv

load_dotenv()

config = types.SimpleNamespace()
config.BOT_TOKEN = os.environ.get('BOT_TOKEN')
config.BOT_PREFIX = os.environ.get('BOT_PREFIX')
config.BOT_STORAGE = os.environ.get('BOT_STORAGE')
config.OPENAI_API_KEY = os.environ.get('OPENAI_API_KEY')
config.COOKIE_PATH = os.environ.get('COOKIE_PATH')
config.STARTUP_MESSAGE = os.environ.get('STARTUP_MESSAGE')
config.STARTUP_COMPLETE_MESSAGE = os.environ.get('STARTUP_COMPLETE_MESSAGE')
config.LANG_MODEL_URL = os.environ.get('LANG_MODEL_URL')