import os
import openai
import json
from config import config
from flask import Flask, redirect, render_template, request, url_for, make_response
app = Flask(__name__)
openai.api_key = os.getenv("OPENAI_API_KEY")


@app.route("/", methods=("GET", "POST"))
def index():
    if request.method == "POST":
        prompt = request.form["prompt"]
        p_data = json.loads(prompt)
        print(p_data)
        responsed = openai.ChatCompletion.create(
            model="gpt-4",
            messages=p_data
        )
        response = make_response(responsed, 200)
        response.mimetype = "application/json"
        print(str(response))
        return response

    result = request.args.get("result")
    return render_template("index.html", result=result)


def generate_prompt(prompt):
    return "{}".format(
        prompt
    )
